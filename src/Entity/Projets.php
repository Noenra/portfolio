<?php

namespace App\Entity;

use App\Repository\ProjetsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjetsRepository::class)]
class Projets
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    private $descrition;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl1;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $imgUrl2;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $lienRepo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $lienSite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescrition(): ?string
    {
        return $this->descrition;
    }

    public function setDescrition(string $descrition): self
    {
        $this->descrition = $descrition;

        return $this;
    }

    public function getImgUrl1(): ?string
    {
        return $this->imgUrl1;
    }

    public function setImgUrl1(?string $imgUrl1): self
    {
        $this->imgUrl1 = $imgUrl1;

        return $this;
    }

    public function getImgUrl2(): ?string
    {
        return $this->imgUrl2;
    }

    public function setImgUrl2(?string $imgUrl2): self
    {
        $this->imgUrl2 = $imgUrl2;

        return $this;
    }

    public function getLienRepo(): ?string
    {
        return $this->lienRepo;
    }

    public function setLienRepo(?string $lienRepo): self
    {
        $this->lienRepo = $lienRepo;

        return $this;
    }

    public function getLienSite(): ?string
    {
        return $this->lienSite;
    }

    public function setLienSite(?string $lienSite): self
    {
        $this->lienSite = $lienSite;

        return $this;
    }
}
